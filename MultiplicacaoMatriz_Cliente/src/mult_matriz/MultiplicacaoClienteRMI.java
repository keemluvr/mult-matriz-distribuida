package mult_matriz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.rmi.Naming;
import java.security.MessageDigest;

public class MultiplicacaoClienteRMI {
	private final static int lin = 4096;
	private final static int col = 4096;
	private final static int nodos = 4;

	private static long[][] matA = new long[lin][col];
	private static long[][] matB = new long[lin][col];
	private static long[][] matC = new long[lin][col];
	private static long[][] linha_A = new long[1024][col];
	private static long[][] linha_B = new long[1024][col];
	private static long[][] linha_C = new long[1024][col];
	private static long[][] linha_D = new long[1024][col];
	private static float[][] respostaA = new float[lin / nodos][col];
	private static float[][] respostaB = new float[lin / nodos][col];
	private static float[][] respostaC = new float[lin / nodos][col];
	private static float[][] respostaD = new float[lin / nodos][col];
	static int cont=4;
	
	//-Djava.security.policy=caminho/MultiplicacaoMatriz_Cliente/src/mult_matriz/cliente.policy

	public static byte[] getFileBytes(File file) {
		int len = (int) file.length();
		byte[] sendBuf = new byte[len];
		FileInputStream inFile = null;

		try {
			inFile = new FileInputStream(file);
			inFile.read(sendBuf, 0, len);
		} catch (Exception e) {
			System.err.print("\n\tErro: " + e.getMessage());
			System.exit(1);
		}

		return sendBuf;
	}

	public static String toHexFormat(final byte[] bytes) {
		final StringBuilder sb = new StringBuilder();
		for (byte b : bytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		int l, c;

		// Carrega a matriz A
		System.out.print("\nLendo arquivo da matriz A...");
		try {
			FileReader file = new FileReader("src/matA.txt");
			BufferedReader bufFile = new BufferedReader(file);

			// Lê a primeira linha
			String line = bufFile.readLine();
			l = c = 0;
			while (line != null) {
				matA[l][c] = Integer.parseInt(line);
				c++;
				if (c >= col) {
					l++;
					c = 0;
				}
				line = bufFile.readLine();

			}
			bufFile.close();
		} catch (Exception e) {
			System.err.print("\n\tErro: " + e.getMessage());
			System.exit(1);
		}

		// Carrega a matriz B
		System.out.print("\nLendo arquivo da matriz B...");
		try {
			FileReader file = new FileReader("src/matB.txt");
			BufferedReader bufFile = new BufferedReader(file);

			// Lê a primeira linha
			String line = bufFile.readLine();
			l = c = 0;
			while (line != null) {
				matB[l][c] = Integer.parseInt(line);
				c++;
				if (c >= col) {
					l++;
					c = 0;
				}
				line = bufFile.readLine();

			}
			bufFile.close();
		} catch (Exception e) {
			System.err.print("\n\tErro 1: " + e.getMessage());
			System.exit(1);
		}

		try {
	
			// Divide a matriz A na primeira parte
			int a = 0;
			for (int i = 0; i < 1024; i++) {
				for (int j = 0; j < col; j++) {
					linha_A[a][j] = matA[i][j];
					
				}
				a++;
			}
			
			// Divide a matriz A na segunda parte
			a=0;
			for (int i = 1024; i < 2048; i++) {
				for (int j = 0; j < col; j++) {
					linha_B[a][j] = matA[i][j];
					
				}
				a++;
			}
			
			// Divide a matriz A na terceira parte
			a=0;
			for (int i = 2048; i < 3072; i++) {
				for (int j = 0; j < col; j++) {
					linha_C[a][j] = matA[i][j];
					
				}
				a++;
			}
			
			// Divide a matriz A na quarta parte
			a=0;
			for (int i = 3072; i < 4096; i++) {
				for (int j = 0; j < col; j++) {
					linha_D[a][j] = matA[i][j];
					
				}
				a++;
			}

			System.setSecurityManager(new SecurityManager());

			// System.setProperty("java.rmi.server.hostname", "localhost");
			// LocateRegistry.createRegistry(2041);

			
			// Envia a primeira parte
			new  Thread() {
				public void run() {
					try {
						
						MultiplicacaoMatrizInterface calcM1;
						calcM1 = (MultiplicacaoMatrizInterface) Naming.lookup("rmi://10.151.33.100:2041/MultiplicacaoMatriz");
						respostaA = calcM1.mult(linha_A, matB);
						cont--;
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					
				}
			}.start();
			
			// Envia a segunda parte
			new  Thread() {
				public void run() {
					try {
						
						MultiplicacaoMatrizInterface calcM2;
						calcM2 = (MultiplicacaoMatrizInterface) Naming.lookup("rmi://localhost:2046/MultiplicacaoMatriz");
						respostaB = calcM2.mult(linha_B, matB);
						cont--;
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					
				}
			}.start();
			
			// Envia a terceira parte
			new  Thread() {
				public void run() {
					try {
						
						MultiplicacaoMatrizInterface calcM3;
						calcM3 = (MultiplicacaoMatrizInterface) Naming.lookup("rmi://10.151.33.112:2043/MultiplicacaoMatriz");
						respostaC = calcM3.mult(linha_C, matB);
						cont--;
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					
				}
			}.start();
			
			// Envia a quarta parte
			new  Thread() {
				public void run() {
					try {
						
						MultiplicacaoMatrizInterface calcM4;
						calcM4 = (MultiplicacaoMatrizInterface) Naming.lookup("rmi://10.151.33.134:2042/MultiplicacaoMatriz");
						respostaD = calcM4.mult(linha_D, matB);
						cont--;
					} catch (Exception e) {
						// TODO: handle exception
					}
					
					
				}
			}.start();
			
			while (cont!=0) {
				System.out.println("Processando");
			}
			
			// Junta a primeira parte na matrizC
			a = 0;
			for (int i = 0; i < 1024; i++) {
				for (int j = 0; j < col; j++) {
					matC[i][j] = (long) respostaA[a][j];
					
				}
				a++;
			}
			
			// Junta a segunda parte na matrizC
			a=0;
			for (int i = 1024; i < 2048; i++) {
				for (int j = 0; j < col; j++) {
					matC[i][j] = (long) respostaB[a][j];
					
				}
				a++;
			}
			
			// Junta a terceira parte na matrizC
			a=0;
			for (int i = 2048; i < 3072; i++) {
				for (int j = 0; j < col; j++) {
					matC[i][j] = (long) respostaC[a][j];
					
				}
				a++;
			}
			
			// Junta a quarta parte na matrizC
			a=0;
			for (int i = 3072; i < 4096; i++) {
				for (int j = 0; j < col; j++) {
					matC[i][j] = (long) respostaD[a][j];
					
				}
				a++;
			}
			

			//Grava um arquivo com a matriz C
			System.out.println("\nGravando arquivo da matriz C...");
			try {
				File fOut = new File("src/matC.txt");
				BufferedWriter writer = new BufferedWriter(new FileWriter(fOut));
				for (int i = 0; i < lin; i++) {
					for (int j = 0; j < col; j++) {
						writer.write(String.valueOf(matC[i][j]));
						if ((i == lin - 1) && (j == col - 1)) {
							continue;
						} else {
							writer.newLine();
						}
					}
				}
				writer.flush();
				writer.close();

			} catch (Exception e) {
				System.err.println("\n\tErro 2: " + e.getMessage());
				System.exit(1);
			}

			
			
			// Gera o MD5
			System.out.print("\nGerando MD5 da matriz C...");
			try {
				File matCFile = new File("src/matC.txt");
				int matCSize = (int) matCFile.length();
				byte[] matCBytes = new byte[matCSize];
				matCBytes = getFileBytes(matCFile);
				MessageDigest md = MessageDigest.getInstance("MD5");
				byte[] hash = md.digest(matCBytes);
				System.out.print("\nHash: " + toHexFormat(hash));
				System.out.print("\nGravando arquivo matC.md5...");
				File md5File = new File("src/matC.md5");
				BufferedWriter writer = new BufferedWriter(new FileWriter(md5File));
				writer.write(toHexFormat(hash) + "   matC.txt");
				writer.flush();
				writer.close();
			} catch (Exception e) {
				System.err.print("\n\tErro 3: " + e.getMessage());
				System.exit(1);
			}

		} catch (Exception e) {
			System.err.println("\tErro 4: " + e.getMessage());
			System.exit(1);
		}

	}
}
