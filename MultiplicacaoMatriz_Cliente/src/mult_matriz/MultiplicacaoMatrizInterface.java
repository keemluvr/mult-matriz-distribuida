package mult_matriz;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MultiplicacaoMatrizInterface extends Remote {
	// Implementação da multiplicação
	public float[][] mult(long linhas_A[][], long matriz_B[][]) throws RemoteException;
}
