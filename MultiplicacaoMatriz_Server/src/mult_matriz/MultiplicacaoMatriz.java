package mult_matriz;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

//Implementação da multiplicação de matrizes
public class MultiplicacaoMatriz extends UnicastRemoteObject implements MultiplicacaoMatrizInterface {

	protected MultiplicacaoMatriz() throws RemoteException {
		super();
	}

	private static final long serialVersionUID = 1L;

	static long inicio = System.currentTimeMillis();

	@Override
	public float[][] mult(long[][] linhas_A, long matriz_B[][]) throws RemoteException {
		float[][] resultado = new float[linhas_A.length][matriz_B[0].length];
		int somatorio = 0;
		System.out.println("---------------------------------------------\nProcessando...");
		resultadoMatriz(linhas_A).toString(); // printa a linha recebida
		resultadoMatriz(matriz_B).toString(); // printa a matriz recebida

		// i - pula as linhas da matriz linhas_A
		// j - pulas cada elemento da colunha da matriz matriz_B
		// k - pula as colunas da matriz_B
		for (int i = 0; i < linhas_A.length; i++) {
			for (int j = 0; j < matriz_B[0].length; j++) {
				for (int k = 0; k < matriz_B.length; k++) {
					somatorio += linhas_A[i][k] * matriz_B[k][j];
				}
				resultado[i][j] = somatorio;
				somatorio = 0; // reseta
				System.out.println("Lendo:\nlinha" + i + "	coluna" + j );
			}
		}

	
		resultadoFinal(resultado).toString(); // printa o resultado final
		System.out.println("Terminado!");

		long fim = System.currentTimeMillis();
		System.out.printf("%.3f ms%n", (inicio - fim) / 1000d);
		return resultado;
	}
	
	/**
	 * Printa o resultado final
	 */
	private String resultadoFinal(float[][] resultado) {
		String teste = "";
		System.out.println("--- Resul ---");
		for (int linha = 0; linha < resultado.length; linha++) {
			for (int coluna = 0; coluna < resultado.length; coluna++) {
				System.out.println("- " + resultado[linha][coluna]);
			}
		}
		return teste;
	}

	/**
	 * Printa uma matriz do tipo long
	 */
	private String resultadoMatriz(long[][] multiplicado) {
		String teste = "";
		System.out.println("--- Matriz ---");
		for (int linha = 0; linha < multiplicado.length; linha++) {
			for (int coluna = 0; coluna < multiplicado.length; coluna++) {
				System.out.println("- " + multiplicado[linha][coluna]);
			}
		}
		return teste;
	}

}
