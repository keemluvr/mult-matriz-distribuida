package mult_matriz;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class MultiplicacaoMatriz_Server {

	// -Djava.security.policy=caminho/MultiplicacaoMatriz_Server/src/mult_matriz/server.policy

	public static void main(String[] args) {
		System.out.print("\nIniciando servidor MultiplicacaoMatriz...");
		
		try {

			// Instancia o gerenciador de segurança
			System.out.print("\n\tRegistrando serviço de segurança");
			System.setSecurityManager(new SecurityManager());

			//System.setProperty("java.rmi.server.hostname", "localhost");
			//LocateRegistry.createRegistry(2041);
			
			// Instancia o objeto local
			MultiplicacaoMatriz multMat = new MultiplicacaoMatriz();

			// Registra o objeto no RMI Registry
			System.out.println("\n\tRegistrando o objeto no RMI registry...");
			Naming.rebind("rmi://localhost:2041/MultiplicacaoMatriz", multMat);

			System.out.println("\n\tAguardando requisições...");
			

		} catch (Exception e) {

			System.err.println("\tErro : " + e.getMessage());
			System.exit(1); // erro

		}

	}

}
