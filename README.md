# Multiplicação de matrizes de forma distribuída

Servidor

 Estrutura 
- Interface do objeto
- Objeto implementado
- Classe para a conexão do cliente
				
 Executando
 - Executa o RMIRegistry  
    No terminal busca a pasta do projeto  
      pasta destino: MultiplicacaoMatriz_Server/bin/  
      comando para executar na pasta: rmiregistry
     
 - Executa o projeto  
     Vai nas confirgurações do projeto  
       Run as -> Run Configurations  
       Aba lateral  
        Java Application
        cola o caminho do projeto
       -Djava.security.policy=caminho/MultiplicacaoMatriz_Server/src/mult_matriz/server.policy
  
Cliente

 Estrutura 
- Interface do objeto
- Objeto implementado
- Classe para a conexão com o servidor
				
 Executando
 - Executa o projeto  
     Vai nas confirgurações do projeto  
       Run as -> Run Configurations  
       Aba lateral  
        Java Application
        cola o caminho do projeto
       -Djava.security.policy=caminho/MultiplicacaoMatriz_Server/src/mult_matriz/cliente.policy